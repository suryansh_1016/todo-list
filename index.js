let itemsLeft = 0;
const taskLeft = document.getElementById("taskLeft");

var newTodo = document.querySelector("input");

newTodo.addEventListener("change", function () {
    var todoMenu = document.getElementById("todo-menu")

    var newCard = document.createElement("li");
    newCard.className = "card";
    todoMenu.appendChild(newCard);
    taskLeft.innerText = `${++itemsLeft} Tasks Left`;

    var chkBox = document.createElement("input");
    chkBox.className = "checkbox"
    chkBox.type = "checkbox";
    newCard.appendChild(chkBox);


    const itemText = document.createElement("p");
    itemText.className = "text";
    itemText.innerText = newTodo.value;
    itemText.setAttribute =
    newCard.appendChild(itemText);
    newTodo.value = "";



    const editButton = document.createElement("i");
    editButton.className = "edit-btn fa-solid fa-pen-to-square";
    newCard.appendChild(editButton);

    editButton.addEventListener("click" , function(){
        itemText.setAttribute.contentEditable = "true";
        itemText.focus();

    })


    itemText.addEventListener("dblclick", function () {
        this.contentEditable = "true";
        this.focus();
    });

    itemText.addEventListener("blur", function () {
        this.contentEditable = "false";
    });

    let flag = 0;
    chkBox.addEventListener("click", function () {
        if (flag == 0) {
            itemText.style.textDecoration = "line-through";
            itemText.style.color = "gray";
            flag = 1;
            taskLeft.innerText = `${--itemsLeft} Tasks Left`
        }
        else {
            itemText.style.textDecoration = "none";
            itemText.style.color = "black";
            flag = 0;
            taskLeft.innerText = `${++itemsLeft} Tasks Left`

        }

    })

    deleteTodo(newCard);
    allTodo(newCard);
    activeTodo(newCard);
    completedTodo(newCard);
    clearCompleted(newCard);

})

function deleteTodo(newCard) {

    const deleteBtn = document.createElement("i");
    deleteBtn.className = "deleteBtn fa-solid fa-xmark";
    newCard.appendChild(deleteBtn);

    deleteBtn.addEventListener("click", function () {
        newCard.remove();
            let cb = newCard.querySelector(".checkbox");
            if (!cb.checked) {
                taskLeft.innerText = `${--itemsLeft} Tasks Left`;
            }
        })
}

function allTodo(newCard) {

    const all = document.querySelector(".all");

        all.addEventListener("click", function () {
            newCard.style.display = "flex";
        })
}




function activeTodo(newCard) {

    let active = document.querySelector(".active");
    let cb = newCard.querySelector(".checkbox");

        active.addEventListener("click", function () {
            if (cb.checked) {
                newCard.style.display = "none";
             }
            else {
            newCard.style.display = "flex";
            }
        })
}




function completedTodo(newCard) {

    let completed = document.querySelector(".completed");
    let cb = newCard.querySelector(".checkbox");

        completed.addEventListener("click", function () {
            if (cb.checked) {
                newCard.style.display = "flex";
            }
            else {
                newCard.style.display = "none";
            }
        })
}


function clearCompleted(newCard) {

    let clear = document.querySelector(".clear-completed");
    let cb = newCard.querySelector(".checkbox");

        clear.addEventListener("click", function () {
            if (cb.checked) {
                newCard.remove();
            }
        })

}









